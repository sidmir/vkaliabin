package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        // TODO : Implement your solution here
        for (int i=0; i<inputNumbers.size(); i++){
            if (inputNumbers.get(i)==null){throw new CannotBuildPyramidException();}
        }
        for (int i=0; i<inputNumbers.size(); i++){
            int count = 0;
            for (int j=0; j<inputNumbers.size(); j++){
                if (inputNumbers.get(i)==inputNumbers.get(j)){count++;}
                if (count>1) {throw new CannotBuildPyramidException();}
            }
        }
        Collections.sort(inputNumbers);
        int f =1, l=2, Height = 1, Lenght = 1;
        while (f < inputNumbers.size()){
            f = f + l;
            l++;
            Height++;
            Lenght +=2;
        }

        if (f != inputNumbers.size()){throw new CannotBuildPyramidException();}
        int [][] pym = new int[Height][Lenght];
        int k = Lenght/2;
        int c = 0,a = 0,m = 0,n = 1,z,v;

        for (int i=0; i<Height; i++){

            for (int j=0; j<Lenght; j++){
                pym[i][j] = 0;}

            a += i;
            c = c + 1 +i;
            v = m;
            z = n;

            for (int g=a; g<c; g++){
                if (i % 2 ==0){
                    pym[i][k-v] = (Integer)inputNumbers.get(g);
                    v-=2;
                }
                if (i % 2!=0){
                    pym[i][k-z] = (Integer)inputNumbers.get(g);
                    z-=2;
                }}

            if (i % 2==0){ m+=2;}
            if (i % 2!=0){ n+=2;}

        }

        return pym;
    }


}
